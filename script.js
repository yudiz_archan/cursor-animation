let shape = document.getElementById("shape");
let twitter = document.getElementById('icon');
let isClick = false;

function moveMouse(pos) {
  shape.style.top = pos.pageY + 'px';
  shape.style.left = pos.pageX + 'px';
}

function moveMouseDown(pos) {
  isClick = true;
  twitter.style.color = "red";
}
function moveMouseUp(pos) {
  if (isClick) {
    twitter.style.color = '#00acee';
    isClick = false;
  }
}

document.addEventListener('mousemove', moveMouse);
document.addEventListener('mousedown', moveMouseDown)
document.addEventListener('mouseup', moveMouseUp)